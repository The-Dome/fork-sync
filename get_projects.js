const {gitlab, stdin, appendFile} = require('./api.js');

stdin.on("line", (id) => {

    gitlab.get(`/users/${id}/projects`)
         .then((res) => {
             let data = res.data;
             data.forEach((project) => {
                 console.log(`Project name: ${project.name}`);
                 console.log(`Git Url: ${project.ssh_url_to_repo}`);
                 if(project.forked_from_project !== null) {
                     appendFile("projects2sync", `${project.id}\n`);
                 }
             })
    })
});
