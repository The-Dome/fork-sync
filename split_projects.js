const {gitlab, stdin, appendFile} = require('./api.js');

stdin.on("line", (id) => {
    gitlab.get(`/projects/${id}/variables/FORK_SYNC_MODE`)
         .then((res) => {
             let data = res.data;
             console.log(`${id}: ${data.value}`);
             splitter(id, data.value);
    }).catch( err => {
        console.log(`${id}: ${process.env.FORK_SYNC_MODE}`);
        splitter(id, process.env.FORK_SYNC_MODE);
    });
});

function splitter(id, FORK_SYNC_MODE) {
    if (FORK_SYNC_MODE === "rebase") {
        appendFile("rebase", `${id}\n`);
    } else if (FORK_SYNC_MODE === "merge_request") {
        appendFile("merge_request", `${id}\n`);
    } else {
        console.error(`Unknown FORK_SYNC_MODE for ${id}. Expected either rebase or merge_request`);
    }
}
