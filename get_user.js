const {gitlab, writeFile} = require('./api.js');

gitlab.get("/user")
     .then((res) => {
         let user = res.data;
         writeFile("user_id", user.id);
         console.log(`Username: ${user.username}`);

         // Why?!
         process.exit();
})
